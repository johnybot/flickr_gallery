package ca.harms.flickrgallery.Modules;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import ca.harms.flickrgallery.FlickrClient;
import ca.harms.flickrgallery.LatestFragment;
import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by johnathanh on 2014-06-24.
 */

@Module(
        injects = LatestFragment.class,
        library = true
)
public class FlickrService {

    @Provides
    FlickrClient.Flickr provideFlickrService() {
        return new RestAdapter.Builder()
                .setEndpoint(FlickrClient.API_URL)
                .setClient(new FakeClient())
                .build()
                .create(FlickrClient.Flickr.class);
    }

    static class FakeClient implements Client {

        private static final int numberOfCats = 30;
        private static String[] catUrls = new String[] {
                "http://www.vetprofessionals.com/catprofessional/images/home-cat.jpg",
                "https://www.petfinder.com/wp-content/uploads/2012/11/99059361-choose-cat-litter-632x475.jpg",
                "http://scienceblogs.com/gregladen/files/2012/12/Beautifull-cat-cats-14749885-1600-1200-590x442.jpg",
                "http://exmoorpet.com/wp-content/uploads/2012/08/cat.png"
        };
        private static int[] catWidths = new int[] {
                512,
                632,
                590,
                276
        };
        private static int[] catHeights = new int[] {
                349,
                475,
                442,
                453
        };

        private static String VALID_RESPONSE = null;

        FakeClient() {
            shuffleCats();
        }

        private void shuffleCats() {
            try {
                JSONObject photos = new JSONObject();
                JSONObject photoWrapper = new JSONObject();
                JSONArray photoArray = new JSONArray();
                Random random = new Random();
                for (int i = 0; i < numberOfCats; i++) {
                    JSONObject photo = new JSONObject();
                    photo.put("id", "" + random.nextInt());
                    photo.put("datetaken", "2014-06-24 15:45:06");
                    photo.put("title", "There are only 4 cats");
                    photo.put("ownername", "Johnathan Harms");

                    int catIndex = random.nextInt(catUrls.length);
                    photo.put("url_m", catUrls[catIndex]);
                    photo.put("width_m", catWidths[catIndex]);
                    photo.put("height_m", catHeights[catIndex]);

                    photo.put("url_l", catUrls[catIndex]);
                    photo.put("width_l", catWidths[catIndex]);
                    photo.put("height_l", catHeights[catIndex]);

                    photo.put("url_o", catUrls[catIndex]);
                    photo.put("width_o", catWidths[catIndex]);
                    photo.put("height_o", catHeights[catIndex]);

                    photo.put("url_c", catUrls[catIndex]);
                    photo.put("width_c", catWidths[catIndex]);
                    photo.put("height_c", catHeights[catIndex]);

                    photoArray.put(photo);
                }
                photoWrapper.put("photo", photoArray);
                photos.put("photos", photoWrapper);

                VALID_RESPONSE = photos.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public Response execute(Request request) throws IOException {
            shuffleCats();
            return new Response(request.getUrl(), 200, "nothing", new ArrayList<Header>(0),
                    new TypedByteArray("application/json", VALID_RESPONSE.getBytes()));
        }
    }
}
