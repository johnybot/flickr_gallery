package ca.harms.flickrgallery;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Johno on 15/02/14.
 */
public abstract class GalleryFragment extends Fragment {
    private static final String TAG = "GalleryFragment";
    protected Callback<FlickrClient.JsonFlickrAPI> mRefreshCallback = new Callback<FlickrClient.JsonFlickrAPI>() {
        @Override
        public void success(FlickrClient.JsonFlickrAPI response, Response response2) {
            Log.d(TAG, "Success");
            mPhotoAdapter.clear();
            mPhotoAdapter.addAll(response.photos.photo);
            mPhotoAdapter.notifyDataSetChanged();
            if (response.photos.photo.isEmpty()) {
                mEmptyText.setText(R.string.no_photos_found);
                mEmptyText.setVisibility(View.VISIBLE);
            }
            mProgressView.setVisibility(View.GONE);
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            mEmptyText.setText(R.string.photo_update_failed);
            mProgressView.setVisibility(View.GONE);
            mEmptyText.setVisibility(View.VISIBLE);
        }
    };
    protected StaggeredGridView mGridView;
    protected PhotoArrayAdapter mPhotoAdapter;
    protected TextView mEmptyText;
    protected ProgressBar mProgressView;
    protected AdapterView.OnItemClickListener mOnSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            PhotoDetailFragment fragment = new PhotoDetailFragment(mPhotoAdapter.getItem(i));

            getFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_photo_browse, container, false);

        mGridView = (StaggeredGridView) rootView.findViewById(R.id.grid_view);
        mEmptyText = (TextView) rootView.findViewById(R.id.empty_view);
        mProgressView = (ProgressBar) rootView.findViewById(R.id.progress_view);
        if (mPhotoAdapter == null) {
            mPhotoAdapter = new PhotoArrayAdapter(getActivity(), R.id.text_view);

            if (savedInstanceState != null) {
                ArrayList<FlickrClient.Photo> serializableArray = (ArrayList<FlickrClient.Photo>) savedInstanceState.getSerializable(TAG);
                if (serializableArray != null) {
                    mPhotoAdapter.addAll(serializableArray);
                    mPhotoAdapter.notifyDataSetChanged();
                }
            }
        }
        mGridView.setOnItemClickListener(mOnSelectedListener);
        mGridView.setAdapter(mPhotoAdapter);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        setHasOptionsMenu(true);
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((PhotoBrowseActivity) getActivity()).showDrawer(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPhotoAdapter != null) {
            ArrayList<FlickrClient.Photo> serializableArray = new ArrayList<FlickrClient.Photo>();
            // TODO: improve performance
            for (int i = 0; i < mPhotoAdapter.getCount(); i++) {
                serializableArray.add(mPhotoAdapter.getItem(i));
            }
            outState.putSerializable(TAG, serializableArray);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.gallery_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    abstract protected void refresh();
}
