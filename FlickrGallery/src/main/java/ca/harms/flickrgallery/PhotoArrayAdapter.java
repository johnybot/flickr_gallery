package ca.harms.flickrgallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by Johno on 14/02/14.
 */
public class PhotoArrayAdapter extends ArrayAdapter<FlickrClient.Photo> {

    static class ViewHolder {
        TextView textView;
        DynamicHeightImageView imageView;
    }

    private LayoutInflater mInfalter;

    public PhotoArrayAdapter(Context context, final int textViewResourceId) {
        super(context, textViewResourceId);
        mInfalter = LayoutInflater.from(context);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInfalter.inflate(R.layout.photo_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView) convertView.findViewById(R.id.text_view);
            viewHolder.imageView = (DynamicHeightImageView) convertView.findViewById(R.id.image_view);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        FlickrClient.Photo photo = getItem(position);

        double ratio = (double) photo.height_m / photo.width_m;
        viewHolder.imageView.setHeightRatio(ratio);
        Picasso.with(getContext())
                .load(photo.url_m)
                .into(viewHolder.imageView);

        viewHolder.textView.setText(photo.title);
        convertView.invalidate();
        return convertView;
    }
}
