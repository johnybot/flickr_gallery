package ca.harms.flickrgallery;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import retrofit.RestAdapter;

/**
 * Created by Johno on 16/02/14.
 */
public class SearchFragment extends GalleryFragment {

    private static final String TAG = "LatestFragment";

    private String mSearchString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        if (savedInstanceState != null) {
            mSearchString = savedInstanceState.getString(TAG);
        }

        if (mPhotoAdapter.isEmpty()) {
            mEmptyText.setVisibility(View.VISIBLE);
            if (mSearchString == null || mSearchString.isEmpty()) {
                mEmptyText.setText(R.string.search_instructions);
            } else {
                mEmptyText.setText(R.string.no_photos_found);
            }
            mEmptyText.setVisibility(View.VISIBLE);
        }

        ((PhotoBrowseActivity) getActivity()).setActionBarTitle(R.string.search_photos_title);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Create a search item in the action bar
        MenuItem item = menu.add(getResources().getString(R.string.search_title));
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        final SearchView searchView = new SearchView(getActivity().getActionBar().getThemedContext());
        searchView.setQuery(mSearchString, false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!TextUtils.isEmpty(s)) {
                    mSearchString = s;
                    refresh();
                    searchView.clearFocus();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // Do nothing until submitted
                return false;
            }
        });
        item.setActionView(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected void refresh() {
        if (!TextUtils.isEmpty(mSearchString)) {
            mEmptyText.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
            mPhotoAdapter.clear();

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int numberOfImages = Integer.parseInt(sharedPref.getString(PreferencesFragment.KEY_PREF_NUMBER_IMAGES, "0"));

            RestAdapter restAdapter = FlickrClient.getFlickrRestAdapter();
            FlickrClient.Flickr flickr = restAdapter.create(FlickrClient.Flickr.class);
            FlickrClient.searchPhotos(flickr, mSearchString, numberOfImages, mRefreshCallback);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, mSearchString);
    }
}
