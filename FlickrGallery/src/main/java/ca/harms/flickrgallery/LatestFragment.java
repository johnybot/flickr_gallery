package ca.harms.flickrgallery;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by Johno on 15/02/14.
 */
public class LatestFragment extends GalleryFragment {
    private static final String TAG = "LatestFragment";

    private ObjectGraph objectGraph;

    @Inject
    FlickrClient.Flickr service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        PhotoBrowseActivity photoBrowseActivity = (PhotoBrowseActivity) getActivity();
        if (photoBrowseActivity != null) {
            photoBrowseActivity.setActionBarTitle(R.string.latest_photos_title);
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if ((getActivity()) != null) {
            ((PhotoBrowseActivity) getActivity()).inject(this);
        }
        if (mPhotoAdapter.getCount() == 0) {
            refresh();
        }
    }

    protected void refresh() {
        mEmptyText.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
        mPhotoAdapter.clear();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int numberOfImages = Integer.parseInt(sharedPref.getString(PreferencesFragment.KEY_PREF_NUMBER_IMAGES, "0"));

        if (service != null) {
            FlickrClient.getLatestPhotos(service, numberOfImages, mRefreshCallback);
        }
    }
}
