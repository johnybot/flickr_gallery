package ca.harms.flickrgallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Johno on 16/02/14.
 */
public class PhotoDetailFragment extends Fragment {

    private static final String TAG = "PhotoDetailFragment";

    private TextView mEmptyText;
    private ProgressBar mProgressView;
    private ImageView mImageView;
    private TextView mUsernameView;
    private TextView mDateView;
    private TextView mDescriptionView;

    private MenuItem mDownloadItem;
    private MenuItem mShareItem;

    private FlickrClient.Photo mPhoto;
    private PhotoViewAttacher mAttacher;
    private ShareActionProvider mShareActionProvider;
    private Bitmap mBitmap;

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            mBitmap = bitmap;
            mImageView.setImageBitmap(mBitmap);
            mProgressView.setVisibility(View.INVISIBLE);
            if (mDownloadItem != null) {
                mDownloadItem.setVisible(mBitmap != null);
            }
            if (mAttacher != null) {
                mAttacher.update();
            }
        }

        @Override
        public void onBitmapFailed(Drawable drawable) {
            mProgressView.setVisibility(View.INVISIBLE);
            mEmptyText.setText(R.string.load_image_failed);
            mEmptyText.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPrepareLoad(Drawable drawable) {
            // Do nothing
        }
    };

    public PhotoDetailFragment() {
        // No argument constructor required
    }

    public PhotoDetailFragment(FlickrClient.Photo photo) {
        mPhoto = photo;
    }

    @Override
    public void onAttach(Activity activity) {
        setHasOptionsMenu(true);
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_photo_detail, container, false);

        mImageView = (ImageView) rootView.findViewById(R.id.photo_view);
        mUsernameView = (TextView) rootView.findViewById(R.id.username);
        mDateView = (TextView) rootView.findViewById(R.id.timestamp);
        mDescriptionView = (TextView) rootView.findViewById(R.id.description);
        mEmptyText = (TextView) rootView.findViewById(R.id.empty_view);
        mProgressView = (ProgressBar) rootView.findViewById(R.id.progress_view);

        if (savedInstanceState != null) {
            setPhoto((FlickrClient.Photo) savedInstanceState.getSerializable(TAG));
        }

        refresh();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Hide the drawer indicator
        ((PhotoBrowseActivity) getActivity()).showDrawer(false);
        mAttacher = new PhotoViewAttacher(mImageView);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAttacher.cleanup();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG, mPhoto);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.details_menu, menu);

        mShareItem = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider) mShareItem.getActionProvider();
        mShareActionProvider.setShareIntent(getShareIntent());
        mShareItem.setVisible(mPhoto != null && !TextUtils.isEmpty(mPhoto.url_l));

        mDownloadItem = menu.findItem(R.id.action_download);
        mDownloadItem.setVisible(mBitmap != null);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            case R.id.action_download:
                // Build custom dialog for image scale
                final NumberPicker numberPicker = new NumberPicker(getActivity());
                numberPicker.setMinValue(10);
                numberPicker.setMaxValue(100);
                numberPicker.setValue(100);
                numberPicker.setWrapSelectorWheel(false);

                final AlertDialog.Builder percentDialog = new AlertDialog.Builder(getActivity());

                percentDialog.setTitle(R.string.percent_dialog_title);
                percentDialog.setView(numberPicker);
                percentDialog.setNegativeButton("Cancel", null);
                percentDialog.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        float percent = numberPicker.getValue() / 100.0f;
                        int scaledWidth = (int) (getImageWidth(mPhoto) * percent);
                        int scaledHeight = (int) (getImageHeight(mPhoto) * percent);
                        File saveFolder = new File(Environment.getExternalStorageDirectory(), "Download");
                        if (!saveFolder.exists()) {
                            saveFolder.mkdirs();
                        }
                        final String fileName = mPhoto.id + ".png";
                        final File output = new File(saveFolder, fileName);
                        ImageSaver imageSaver = new ImageSaver(scaledWidth, scaledHeight, output);
                        imageSaver.execute(mBitmap);
                    }
                });
                percentDialog.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Intent getShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, getImageURL(mPhoto));
        return intent;
    }

    public void setPhoto(FlickrClient.Photo photo) {
        if (mPhoto == null || !mPhoto.equals(photo)) {
            mPhoto = photo;
            if (mShareItem != null) {
                mShareItem.setVisible(mPhoto != null && !TextUtils.isEmpty(mPhoto.url_l));
            }
            refresh();
        }
    }

    private void refresh() {
        if (mPhoto != null && mImageView != null) {
            String url = getImageURL(mPhoto);
            if (url == null) {
                // No url available
                mEmptyText.setText(R.string.load_image_no_url);
                mEmptyText.setVisibility(View.VISIBLE);
            } else {
                // Download image
                mProgressView.setVisibility(View.VISIBLE);
                mEmptyText.setVisibility(View.GONE);
                Picasso.with(getActivity())
                        .load(mPhoto.url_l)
                        .into(target);

                mUsernameView.setText(mPhoto.ownername);
                mDescriptionView.setText(mPhoto.title);

                SimpleDateFormat formater = new SimpleDateFormat(FlickrClient.DATE_FORMAT);
                try {
                    Date parsed = formater.parse(mPhoto.datetaken);
                    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getActivity());
                    DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(getActivity());

                    String timeString = timeFormat.format(parsed);
                    String dateString = dateFormat.format(parsed);

                    mDateView.setText(timeString + " : " + dateString);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        } else {
            // No photo or ImageView
            mEmptyText.setText(R.string.load_image_failed);
            mEmptyText.setVisibility(View.VISIBLE);
        }
    }

    private String getImageURL(FlickrClient.Photo mPhoto) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sizeWanted = sharedPref.getString(PreferencesFragment.KEY_PREF_IMAGE_SIZE, "0");

        return mPhoto.getBestUrl(sizeWanted);
    }

    private int getImageHeight(FlickrClient.Photo mPhoto) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sizeWanted = sharedPref.getString(PreferencesFragment.KEY_PREF_IMAGE_SIZE, "0");

        return mPhoto.getHeight(sizeWanted);
    }

    private int getImageWidth(FlickrClient.Photo mPhoto) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sizeWanted = sharedPref.getString(PreferencesFragment.KEY_PREF_IMAGE_SIZE, "0");

        return mPhoto.getWidth(sizeWanted);
    }

    private class ImageSaver extends AsyncTask<Bitmap, Void, Boolean> {

        public int scaledWidth = 1;
        public int scaledHeight = 1;
        public File locationToSave;

        public ImageSaver(int scaledWidth, int scaledHeight, File locationToSave) {
            this.scaledWidth = scaledWidth;
            this.scaledHeight = scaledHeight;
            this.locationToSave = locationToSave;
        }

        @Override
        protected Boolean doInBackground(Bitmap... bitmaps) {
            if (bitmaps.length < 1 || locationToSave == null) {
                return false;
            }
            Bitmap originalBitmap = bitmaps[0];
            if (originalBitmap == null) {
                return false;
            }
            FileOutputStream fos = null;
            try {
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(mBitmap, scaledWidth, scaledHeight, false);
                fos = new FileOutputStream(locationToSave);
                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

                fos.flush();
                fos.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (getActivity() != null) {
                if (aBoolean) {
                    Toast.makeText(getActivity(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
