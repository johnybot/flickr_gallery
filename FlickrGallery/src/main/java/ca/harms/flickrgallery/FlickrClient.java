package ca.harms.flickrgallery;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Johno on 14/02/14.
 */

public class FlickrClient {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    // REST Url for Flickr
    public static final String API_URL = "http://api.flickr.com/services/rest";
    private static final String API_KEY = "76f6739e65bcaa008e73b313c852656a";
    // Methods
    private static final String LATEST_METHOD = "flickr.photos.getRecent";
    private static final String SEARCH_METHOD = "flickr.photos.search";
    // Format
    private static final String JSON_FORMAT = "json";
    // Extras
    private static final String EXTRAS = "url_m, url_l, url_o, url_c, date_taken, owner_name";
    // Photo sizes
    private static final String MEDIUM = "Medium";
    private static final String LARGE = "Large";
    private static final String ORIGINAL = "Original";
    // Sorting method
    private static final String RELEVANCE = "relevance";

    public static void getLatestPhotos(Flickr flickr, int numberOfImages, Callback<JsonFlickrAPI> callback) {
        flickr.recentPhotos(LATEST_METHOD,
                API_KEY,
                JSON_FORMAT,
                true,
                EXTRAS,
                numberOfImages,
                callback);
    }

    public static void searchPhotos(Flickr flickr, String searchText, int numberOfImages, Callback<JsonFlickrAPI> callback) {
        flickr.search(SEARCH_METHOD,
                API_KEY,
                JSON_FORMAT,
                true,
                EXTRAS,
                numberOfImages,
                searchText,
                RELEVANCE,
                callback);
    }

    public static RestAdapter getFlickrRestAdapter() {
        return new RestAdapter.Builder()
                .setEndpoint(FlickrClient.API_URL)
                .build();
    }

    public interface Flickr {
        @GET("/")
        void recentPhotos(
                @Query("method") String method,
                @Query("api_key") String apiKey,
                @Query("format") String format,
                @Query("nojsoncallback") boolean noJsonCallback,
                @Query("extras") String extras,
                @Query("per_page") int perPage,
                Callback<JsonFlickrAPI> cb
        );

        @GET("/")
        void search(
                @Query("method") String method,
                @Query("api_key") String apiKey,
                @Query("format") String format,
                @Query("nojsoncallback") boolean noJsonCallback,
                @Query("extras") String extras,
                @Query("per_page") int perPage,
                @Query("text") String searchText,
                @Query("sort") String sort,
                Callback<JsonFlickrAPI> cb
        );
    }

    public static class JsonFlickrAPI {
        PhotoWrapper photos;
    }

    public static class PhotoWrapper {
        List<Photo> photo;
    }

    public static class Photo implements Serializable {
        public String id;
        public String title;
        public String datetaken;
        public String ownername;

        public String url_m;
        public int height_m;
        public int width_m;

        // large, 1024 on longest side
        public String url_l;
        public int height_l;
        public int width_l;

        // original image, either a jpg, gif or png, depending on source format
        public String url_o;
        public int height_o;
        public int width_o;

        // medium 800, 800 on longest side
        public String url_c;
        public int height_c;
        public int width_c;

        // Get the best url with the supplied preference
        public String getBestUrl(String imageSize) {
            String url = null;
            // Grab the url asked for
            if (imageSize.equals(MEDIUM)) {
                url = url_c;
            } else if (imageSize.equals(LARGE)) {
                url = url_l;
            } else if (imageSize.equals(ORIGINAL)) {
                url = url_o;
            }
            // if found
            if (!TextUtils.isEmpty(url)) {
                return url;
            }
            // If not found grab largest available
            if (url_o != null) {
                url = url_o;
            } else if (url_l != null) {
                url = url_l;
            } else if (url_c != null) {
                url = url_c;
            } else if (url_m != null) {
                url = url_m;
            }
            return url;
        }

        // Same algorithm as getBestUrl
        public int getHeight(String imageSize) {
            int height = 0;
            if (imageSize.equals(MEDIUM)) {
                height = height_c;
            } else if (imageSize.equals(LARGE)) {
                height = height_l;
            } else if (imageSize.equals(ORIGINAL)) {
                height = height_o;
            }

            if (height != 0) {
                return  height;
            }

            if (height_o != 0) {
                height = height_o;
            } else if (height_l != 0) {
                height = height_l;
            } else if (height_c != 0) {
                height = height_c;
            } else if (height_m != 0) {
                height = height_m;
            }

            return height;
        }

        // Same algorithm as getBestUrl
        public int getWidth(String imageSize) {
            int width = 0;
            if (imageSize.equals(MEDIUM)) {
                width = width_c;
            } else if (imageSize.equals(LARGE)) {
                width = width_l;
            } else if (imageSize.equals(ORIGINAL)) {
                width = width_o;
            }
            
            if (width != 0) {
               return  width;
            }

            if (width_o != 0) {
                width = width_o;
            } else if (width_l != 0) {
                width = width_l;
            } else if (width_c != 0) {
                width = width_c;
            } else if (width_m != 0) {
                width = width_m;
            }
            
            return width;
        }

        @Override
        public boolean equals(Object compareObject) {
            if (compareObject instanceof Photo) {
                Photo comparePhoto = (Photo) compareObject;
                if (id.equals(comparePhoto.id)) {
                    return true;
                }
            }
            return false;
        }
    }
}
