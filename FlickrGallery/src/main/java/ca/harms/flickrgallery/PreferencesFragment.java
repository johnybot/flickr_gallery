package ca.harms.flickrgallery;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

/**
 * Created by Johno on 18/02/14.
 */
public class PreferencesFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_PREF_NUMBER_IMAGES = "pref_max_images";
    public static final String KEY_PREF_IMAGE_SIZE = "pref_image_size";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();

        // Update the summaries to their stored values
        Preference numberImagesPref = findPreference(KEY_PREF_NUMBER_IMAGES);
        numberImagesPref.setSummary(sp.getString(KEY_PREF_NUMBER_IMAGES, ""));

        Preference imageSizePref = findPreference(KEY_PREF_IMAGE_SIZE);
        imageSizePref.setSummary(sp.getString(KEY_PREF_IMAGE_SIZE, ""));

        ((PhotoBrowseActivity) getActivity()).setActionBarTitle(R.string.preferences_title);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((PhotoBrowseActivity) getActivity()).showDrawer(false);
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // Update the summaries to their current value
        if (key.equals(KEY_PREF_NUMBER_IMAGES)) {
            Preference numberImagesPref = findPreference(key);
            numberImagesPref.setSummary(sharedPreferences.getString(key, ""));
        } else if (key.equals(KEY_PREF_IMAGE_SIZE)) {
            Preference imageSizePref = findPreference(key);
            imageSizePref.setSummary(sharedPreferences.getString(key, ""));
        }
    }
}
