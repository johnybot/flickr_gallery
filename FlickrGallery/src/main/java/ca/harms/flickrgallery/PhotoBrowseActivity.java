package ca.harms.flickrgallery;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import ca.harms.flickrgallery.Modules.FlickrService;
import dagger.ObjectGraph;

public class PhotoBrowseActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = "PhotoBrowseActivity";
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private String mTitle;

    private ObjectGraph serviceGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_browse);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        serviceGraph = ObjectGraph.create(new FlickrService());

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.photo_browse_activity, menu);
            restoreActionBar();
            getFragmentManager().findFragmentById(R.id.container).setHasOptionsMenu(true);
            return true;
        } else {
            getFragmentManager().findFragmentById(R.id.container).setHasOptionsMenu(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public void setActionBarTitle(int title) {
        mTitle = getString(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            setTitle(R.string.preferences_title);
            getFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.container, new PreferencesFragment())
                .addToBackStack(null)
                .commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment = null;
        // Clear the backstack
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        switch (position) {
            case 0:
                fragment = new LatestFragment();
                mTitle = getString(R.string.latest_photos_title);
                break;
            case 1:
                fragment = new SearchFragment();
                mTitle = getString(R.string.search_photos_title);
                break;
        }
        if (fragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();

        }
    }

    public void showDrawer(boolean showDrawer) {
        mNavigationDrawerFragment.setDisplayDrawer(showDrawer);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void inject(Object object) {
        serviceGraph.inject(object);
    }
}
