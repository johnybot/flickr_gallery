package ca.harms.flickrgallery.test;


import junit.framework.TestCase;

import org.junit.Test;

import ca.harms.flickrgallery.FlickrClient;


/**
 * Created by Johno on 19/02/14.
 */
public class PhotoTest extends TestCase{
    FlickrClient.Photo photo;

    @Override
    public void setUp() throws Exception {
        photo = new FlickrClient.Photo();
        photo.url_c = "url_c";
        photo.height_c = 10;
        photo.width_c = 20;
        photo.url_o = "url_o";
        photo.height_o = 40;
        photo.width_o = 70;
    }

    @Override
    public void tearDown() throws Exception {
    }

    @Test
    public void testHasUrlO() throws Exception {
        String url_o = photo.getBestUrl("Original");
        assertTrue("url_o".equals(url_o));
        assertFalse("url_l".equals(url_o));
        assertFalse("url_c".equals(url_o));
    }
    @Test
    public void testDoesntHaveUrlL() throws Exception {
        String url_l = photo.getBestUrl("Large");
        assertTrue("url_o".equals(url_l));
        assertFalse("url_l".equals(url_l));
        assertFalse("url_c".equals(url_l));
    }
    @Test
    public void testHasUrlC() throws Exception {
        String url_c = photo.getBestUrl("Medium");
        assertTrue("url_c".equals(url_c));
        assertFalse("url_l".equals(url_c));
        assertFalse("url_o".equals(url_c));
    }
}
