package ca.harms.flickrgallery.test;

import android.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ListView;

import com.robotium.solo.Solo;

import org.junit.Test;

import ca.harms.flickrgallery.PhotoBrowseActivity;
import ca.harms.flickrgallery.R;
import ca.harms.flickrgallery.SearchFragment;

import static org.junit.Assert.*;

/**
 * Created by Johno on 19/02/14.
 */

public class SearchFragmentTest extends ActivityInstrumentationTestCase2<PhotoBrowseActivity> {

    private Solo solo;

    public SearchFragmentTest() {
        super(PhotoBrowseActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
        solo.clickOnText(solo.getString(R.string.search_title));
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
    @Test
    public void testInstructions() throws Exception {
        View emptyView = solo.getView(R.id.empty_view);
        assertTrue(solo.searchText(solo.getString(R.string.search_instructions)));
    }
    @Test
    public void testFragment() throws Exception {
        Fragment fragment = solo.getCurrentActivity().getFragmentManager().findFragmentById(R.id.container);
        assertTrue(fragment instanceof SearchFragment);
    }
}
