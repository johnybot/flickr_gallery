package ca.harms.flickrgallery.test;

import android.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import com.robotium.solo.Solo;

import org.junit.Test;

import ca.harms.flickrgallery.LatestFragment;
import ca.harms.flickrgallery.PhotoBrowseActivity;
import ca.harms.flickrgallery.PhotoDetailFragment;
import ca.harms.flickrgallery.R;

import static org.junit.Assert.*;

/**
 * Created by Johno on 19/02/14.
 */

public class LatestFragmentTest extends ActivityInstrumentationTestCase2<PhotoBrowseActivity> {

    private Solo solo;

    public LatestFragmentTest() {
        super(PhotoBrowseActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
    @Test
    public void testLoading() throws Exception {
        View loadingView = solo.getView(R.id.progress_view);
        assertTrue(loadingView.getVisibility() == View.VISIBLE);
    }
    @Test
    public void testFragment() throws Exception {
        Fragment fragment = solo.getCurrentActivity().getFragmentManager().findFragmentById(R.id.container);
        assertTrue(fragment instanceof LatestFragment);
    }
}
