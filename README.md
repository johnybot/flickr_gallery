FlickrGallery
=========

FlickrGallery is a Android application that follows the outline given by HootSuite. It's intent is to show my Technical capabilities as an Android developer.

FlickrGallery allows you to view the latest photos uploaded to Flickr. You can also search for images using a text string

Developer
---------

##### Johnathan Harms

Extra Features
--------------

- Choose size of photo to view
- Customize the amount of images fetched
- Navigation drawer
- Share image URL using standard share menu
- Search for Images
- Pinch and doubletap to zoom
- Robotium test cases that test user interaction

Issues
------

- Test cases are included but they do not cover as much of the Application as I would like
- Photos sometimes do not download. Picasso will continue trying for a very long time.
- Saving the image to Disk done in an AsyncTask and shows a toast when completed. Ideally this would be done using a Service and display a Notification when completed.
- StaggeredGridView has a grid_paddingTop attribute. This doesn't seem to offset the grid like it should

Libraries Used
--------------

- JUnit 
  - http://junit.org/
- Robotium 
  - https://code.google.com/p/robotium/
- StaggeredGridView
  - https://github.com/etsy/AndroidStaggeredGrid
- Retrofit
  - http://square.github.io/retrofit/
- Picasso
  - http://square.github.io/picasso/
- PhotoView
  - https://github.com/chrisbanes/PhotoView

Original Specification
----------------------

> ### Summary
>
> Develop a simple photo viewing application that leverages Flickr's API detailed here: http://www.flickr.com/services/api/

> ### Main Goal
> 
> The app should be able to display a list of photos which are selectable. When chosen, a different view is displayed with a larger version of that image along with extra meta-data about that image (ie: date taken, location, etc)

> ### Design Notes
>
> - The app should be designed with the following in mind as baseline requirements:
> -The app should be created using Android Studio and build using the Gradle build system
> - Please use a private GIT source control service like BitBucket so we can look at your commit history and review your development process. We will provide emails for you to invite your reviewers to gain access to the repo.
> - The minSDK level should be 14 (Ice Cream Sandwich) with an appropriate target SDK 
> - The app should implement an ActionBar with appropriate menu options
> - There should be a simple settings screen with a couple of configurable options the user can make that change the app's behaviour
> - The app should work on any size Android device with particular attention paid to 4-5" phone, 7-8" tablet and 10+" tablet. Layouts should be customized for those three buckets to take advantage of the extra screen space.
> - The app should support rotation on both phone and tablet
> - While third party libraries are recommended to handle various components of the app, please use the Flickr APIs directly via your choice of network solution to demonstrate your understanding of web service calls. (ie: No Flickr helper libraries!)
> - You should implement 3 unit tests using the Gradle supported Android Instrument Test tools. These tests should drive different pieces of your UI to confirm things are working correctly. (ie: Press a button and confirm the correct action taken)
> - Provide a menu option to save a selected image to the SD card. There should be an option to scale the image to a different percentage of its original full size.

> The app should also have the following to improve the user experience:
> - The list of photos should scroll smoothly and transition animations should be applied 
> - Tasteful animations of the content using the most appropriate animation techniques for the minSDK level 14

> ### Review Criteria

> To be determined successful the application will have:
> - Adhered to the baseline requirements and have produced a functional application according to the specifications
> - Shown a consistent coding standard throughout the application
> - Leveraged available open source libraries to reduce development time
> - The source repo should show a clear implementation strategy 

> To be deemed excellent, the application will have:
> - Have little to no "jank" and be as "buttery" as possible
> - Little tweaks and tasteful animations used where possible
> - Added features above and beyond the baseline requirements